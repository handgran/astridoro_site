<section id="orcamento">
    <?php $section = get_page_by_path('orcamento',OBJECT,'conteudo'); ?>
    <div class="row">
        <div class="columns large-offset-1 large-7">
            <span>
                <?= $section->post_content; ?>
            </span>
        </div>
        <div class="columns large-3 end">
            <a href="http://www.astridoro.com.br/orcamento/">ORÇAMENTO</a>
        </div>
    </div>
</section>
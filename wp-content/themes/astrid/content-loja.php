<section id="loja">
    <?php $section = get_page_by_path('loja-virtual',OBJECT,'conteudo'); ?>
    <header class="row">
        <div class="columns large-12">
            <h3><?= $section->post_title; ?></h3>
            <p><?= $section->post_content; ?></p>
        </div>
        <div class="row">
            <div class="columns large-2 large-offset-5">
                <!-- <a href="#" class="link-loja">CONHECER</a> -->
            </div>
        </div>
    </header>
</section>
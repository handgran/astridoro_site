<?php
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'conteudo',
    array(
      'labels' => array(
        'name' => __( 'Conteudos' ),
        'singular_name'      => __( 'Conteúdo' ),
        'add_new'            => __( 'Novo conteúdo' ),
        'add_new_item'       => __( 'Novo conteúdo' ),
        'edit_item'          => __( 'Editar conteúdo' ),
        'new_item'           => __( 'Conteúdo' ),
        'view_item'          => __( 'Ver conteúdo' ),  
        'search_items'       => __( 'Procurar conteúdo' ),
        'not_found'          => __( 'Nenhum Conteúdo encontrado' ),
        'not_found_in_trash' => __( 'Nenhum Conteúdo na lixeira' ),
      ),
      'taxonomies' => array('conteudo_page'),
      'public' => true,
      'has_archive' => false,
      'supports' => array(
            'title', 'editor', 'thumbnail'
      )
    )
  );
  
  register_post_type( 'slider',
    array(
      'labels' => array(
        'name' => __( 'Sliders' ),
        'singular_name'      => __( 'Slider' ),
        'add_new'            => __( 'Novo slider' ),
        'add_new_item'       => __( 'Novo slider' ),
        'edit_item'          => __( 'Editar slider' ),
        'new_item'           => __( 'Slider' ),
        'view_item'          => __( 'Ver slider' ),  
        'search_items'       => __( 'Procurar slider' ),
        'not_found'          => __( 'Nenhum Slider encontrado' ),
        'not_found_in_trash' => __( 'Nenhum Slider na lixeira' ),
      ),
      'public' => true,
      'has_archive' => false,
      'supports' => array(
            'title', 'editor', 'thumbnail'
      )
    )
  );
  
  register_post_type( 'cliente',
    array(
      'labels' => array(
        'name' => __( 'Clientes' ),
        'singular_name'      => __( 'Clientes' ),
        'add_new'            => __( 'Novo cliente' ),
        'add_new_item'       => __( 'Novo cliente' ),
        'edit_item'          => __( 'Editar cliente' ),
        'new_item'           => __( 'Cliente' ),
        'view_item'          => __( 'Ver cliente' ),  
        'search_items'       => __( 'Procurar cliente' ),
        'not_found'          => __( 'Nenhum Cliente encontrado' ),
        'not_found_in_trash' => __( 'Nenhum Cliente na lixeira' ),
      ),
      'public' => true,
      'has_archive' => false,
      'supports' => array(
            'title', 'thumbnail'
      )
    )
  );
  
  register_post_type( 'produto',
    array(
      'labels' => array(
        'name' => __( 'Produtos' ),
        'singular_name'      => __( 'Produtos' ),
        'add_new'            => __( 'Novo produto' ),
        'add_new_item'       => __( 'Novo produto' ),
        'edit_item'          => __( 'Editar produto' ),
        'new_item'           => __( 'Produto' ),
        'view_item'          => __( 'Ver produto' ),  
        'search_items'       => __( 'Procurar produto' ),
        'not_found'          => __( 'Nenhum Produto encontrado' ),
        'not_found_in_trash' => __( 'Nenhum Produto na lixeira' ),
      ),
      'supports' => array(
            'title', 'thumbnail'
      ),
      'rewrite' => array( 'slug' => 'produto', 'with_front' => false ),
      'taxonomies' => array('produto_page'),
      'public' => true,
      'has_archive' => false
    )
  );
  
}

add_theme_support( 'post-thumbnails', array( 'post', 'page', 'conteudo' , 'cliente' , 'produto' , 'slider') );

add_action( 'init', 'create_slider_tax' );

function create_slider_tax() {
        
        register_taxonomy(
		'conteudo_page',
		'conteudo',
		array(
			'label' => __( 'Conteúdo' ),
                        'labels' => array(
                            'name' => 'Tipo de Conteúdo',
                            'add_new_item' => 'Adicionar novo tipo de conteúdo',
                            'new_item_name' => "Novo tipo de conteúdo"
                        ),
			'rewrite' => array( 'slug' => 'conteudo_page' ),
			'hierarchical' => true,
                        'show_ui' => true,
                        'show_tagcloud' => false,
                        'hierarchical' => true
		)
	); 
        
        register_taxonomy(
		'produto_page',
		'produto',
		array(
			'label' => __( 'Produto' ),
                        'labels' => array(
                            'name' => 'Tipo do Produto',
                            'add_new_item' => 'Adicionar novo tipo de produto',
                            'new_item_name' => "Novo tipo de produto"
                        ),
			'rewrite' => array('slug' => 'produtos', 'with_front' => true, 'hierarchical' => false ),
                        'show_ui' => true,
                        'show_tagcloud' => true,
                        'hierarchical' => true,
                        'has_archive' => true,
                        'public' => true
		)
	);
}

/**
 * Display a custom taxonomy dropdown in admin
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'conteudo'; // change to your post type
	$taxonomy  = 'conteudo_page'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Exibir {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'conteudo'; // change to your post type
	$taxonomy  = 'conteudo_page'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}


/**
 * Display a custom taxonomy dropdown in admin
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy_produto');
function tsm_filter_post_type_by_taxonomy_produto() {
	global $typenow;
	$post_type = 'produto'; // change to your post type
	$taxonomy  = 'produto_page'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Exibir {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'tsm_convert_id_to_term_in_query_produto');
function tsm_convert_id_to_term_in_query_produto($query) {
	global $pagenow;
	$post_type = 'produto'; // change to your post type
	$taxonomy  = 'produto_page'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}

flush_rewrite_rules( false );


add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}


/* 
// this can live in /themes/mytheme/functions.php, or maybe as a dev plugin?
function get_template_name () {
	foreach ( debug_backtrace() as $called_file ) {
		foreach ( $called_file as $index ) {
			if ( !is_array($index[0]) AND strstr($index[0],'/themes/') AND !strstr($index[0],'footer.php') ) {
				$template_file = $index[0] ;
			}
		}
	}
	$template_contents = file_get_contents($template_file) ;
	preg_match_all("(Template Name:(.*)\n)siU",$template_contents,$template_name);
	$template_name = trim($template_name[1][0]);
	if ( !$template_name ) { $template_name = '(default)' ; }
	$template_file = array_pop(explode('/themes/', basename($template_file)));
	return $template_file . ' > '. $template_name ;
}

*/





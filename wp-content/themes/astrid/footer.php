<footer class="main">
    <div class="row">
        <div class="columns large-3 small-12">
            <img class="logo" src="<?php bloginfo('template_url'); ?>/_public/img/logo-footer.png">
        </div>
        <div class="columns large-2 informacoes">
            <h5>Informações</h5>
            <nav>
                <a href="http://www.astridoro.com.br/quem-somos/">A empresa</a>
                <a href="http://www.astridoro.com.br/FAQ/">Suporte / FAQ</a>
                <a href="http://www.astridoro.com.br/orcamento/">Fazer pedido</a>
                <a href="http://www.astridoro.com.br/#contato">Trabalhe conosco</a>
            </nav>
        </div>
        <div class="columns large-7 newsletter">
            <?php $section = get_page_by_path('newsletter',OBJECT,'conteudo'); ?>
            <h5><?= $section->post_title; ?></h5>
            <?= apply_filters('the_content', $section->post_content); ?>
        </div>
    </div>
</footer>
<div class="min-footer">
    <div class="row">
        <div class="large-12">
            <span>Copyright © 2018 Astrid’Oro - Direitos reservados.</span>
            <span>Desenvolvido por <a href="http://handgran.com">Handgran</a></span>
        </div>
    </div>
</div>

<!--

<?php var_dump(is_post_type_archive()); ?> is_post_type_archive
<?php var_dump(is_page()); ?> is_page
<?php var_dump(is_tax()); ?> is_tax
<?php var_dump(is_category()); ?> is_category
<?php var_dump(is_home()); ?> is_home
<?php var_dump(is_archive()); ?> is_archive
<?php var_dump(is_trackback()); ?> is_trackback
<?php var_dump(is_front_page()); ?> is_front_page
<?php var_dump(is_singular()); ?> is_singular
<?php var_dump(is_404()); ?> is_404

-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74086119-1', 'auto');
  ga('send', 'pageview');

</script>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <?php get_header(); ?>
    </head>
    <body>
        <header class="main">
            
            <?php get_template_part( 'content', 'responsive-menu' );  ?>
            
            <?php get_template_part( 'content', 'top-bar' );  ?>
            
            <div class="front-of-slider">
                <div class="row">
                    <div class="columns large-12">
                        <h1 class="logo"><span>Astrid Oro</span></h1>
                    </div>
                </div>
            </div>

           <!--   <?php
                  $sliders = get_posts(array(
                    'numberposts'   => 6,
                    'post_type' => '$slider'
                ));
                 $i =0; 
                  foreach ($sliders as $sliders) {
                    if ($i == 0) {
                       $display = "display:none!important";
                       var_dump($display);
                    }else{ $display = "display:block!important"; var_dump($display);}
                     
                 $i++; }
            ?> -->
            
           <!--  <div class="banners-area">
                <div class="banners">
                    <?php
                    $sliders = get_posts(array(
                        'numberposts'	=> 6,
                        'post_type'	=> '$slider'
                    ));
                ?>
                    <?php if($sliders): ?>
                    <?php foreach($sliders as $slider): ?>
                        <div class="banner">
                            <div class="banner-conteudo">
                                <h2><?= $slider->post_title ?></h2>
                                <p><?= $slider->post_content ?></p>
                            </div>
                            <img class="lazy-img" src="" data-src="<?= wp_get_attachment_url( get_post_thumbnail_id($slider->ID) ); ?>" alt="<?= $slider->post_title; ?>"> 
                            <div class="banners-mask"></div>
                        </div>
                    <?php endforeach; endif; ?>
                    
                </div>
                
            </div> -->
            
        

            <div class="bannerArea">
           
            <?php if($sliders): ?>
                <?php foreach($sliders as $slider): ?>
                    <div class="bannerTopo">
                        <div class="bannerTopo-conteudo">
                            <h2><?= $slider->post_title ?></h2>
                            <p><?= $slider->post_content ?></p>
                        </div>
                        <img class="lazy-img" src="" data-src="<?= wp_get_attachment_url( get_post_thumbnail_id($slider->ID) ); ?>" alt="<?= $slider->post_title; ?>"> 
                    </div>
                <?php endforeach; endif; ?>
            </div>

            <nav class="menu-principal">
                <div class="row">
                    <div class="columns text-center large-11 large-offset-1">
                        <a href="http://www.astridoro.com.br">Principal</a>
                        <a href="http://www.astridoro.com.br/quem-somos/">Quem somos</a>
                        <a href="http://www.astridoro.com.br/produtos/">Produtos</a>
                        <a href="#"><img src="<?php bloginfo('template_url'); ?>/_public/img/sol-menu.png"></a>
                        <!-- <a href="#">Loja Astrid’Oro BIJOUX</a> -->
                        <a href="http://www.astridoro.com.br/orcamento/">Orçamento</a>
                        <a href="http://www.astridoro.com.br/#contato">Contato</a>
                        <a href="http://www.astridoro.com.br/FAQ/">Suporte</a>
                    </div>
                </div>
            </nav>
        </header>
        <section id="brindes">
            <header class="row">
                <?php $section = get_page_by_path('brindes-personalizados',OBJECT,'conteudo'); ?>
                <div class="columns large-12 small-12">
                    <h3><?= $section->post_title ?></h3>
                </div>
            </header>
            <div class="row">
                <?php
                    $brindes = get_posts(array(
                    'numberposts'	=> 4,
                    'post_type'		=> 'conteudo',
                    'conteudo_page'     => 'brindes_personalizados',
                    'meta_value'	=> 1
                    ));
                ?>
                <?php if($brindes): ?>
                    <?php foreach($brindes as $brinde): ?>
                            <article class="columns large-3 brinde">
                                <figure>
                                    <img class="lazy-img" src="" data-src="<?= wp_get_attachment_url( get_post_thumbnail_id($brinde->ID) ); ?>" alt="<?= $brinde->post_title; ?>" />
                                </figure>
                                <h4><?= $brinde->post_title; ?></h4>
                                <?= apply_filters('the_content', $brinde->post_content); ?>
                            </article>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </section>
        
        <?php get_template_part( 'content', 'orcamento' ); // elemento compartilhado servicos ?>
        
        <section id="empresa">
            <?php $section = get_page_by_path('a-empresa',OBJECT,'conteudo'); ?>
            <header class="row">
                <div class="columns large-12">
                    <h3><?= $section->post_title ?></h3>
                </div>
            </header>
            <div class="row">
                <div class="columns large-offset-2 large-6">
                    <p>
                        <?= $section->post_content ?>
                    </p>
                </div>
                <div class="columns large-4">
                    <img class="lazy-img" src="<?php bloginfo('template_url'); ?>/selo.png" style="max-width: 173px;"  alt="Selo 30 anos" />
                </div>
            </div>
        </section>
        
        <section id="tratamentos">
            <?php $section = get_page_by_path('acabamentos',OBJECT,'conteudo'); ?>
            <div class="row tratamentos">
                <div class="columns large-6 tratamentos-img">
                    <img class="lazy-img" src="" data-src="<?= wp_get_attachment_url( get_post_thumbnail_id($section->ID) ); ?>" alt="<?= $section->post_title ?>">
                </div>
                <div class="columns large-6 small-12">
                    <header>
                            <h3><?= $section->post_title ?></h3>
                    </header>
                    <div class="tratamentos-lista">
                        <?php
                            $tratamentos = get_posts(array(
                            'numberposts'	=> -1,
                            'post_type'		=> 'conteudo',
                            'conteudo_page'     => 'acabamentos',
                            'meta_key'          => "ordem",
                            'orderby'		=> 'meta_value_num',
                            'order'		=> 'ASC',
                            ));
                        ?>
                        <?php if($tratamentos): ?>
                        <?php foreach($tratamentos as $tratamento): ?>
                            <article>
                                <h4>
                                    <span><?= get_field("opcao01", $tratamento->ID); ?></span>
                                    <figure><img class="lazy-img" src="" data-src="<?= wp_get_attachment_url( get_post_thumbnail_id($tratamento->ID) ); ?>" alt="<?= $tratamento->post_title ?>"></figure>
                                    <span><?= get_field("opcao02", $tratamento->ID); ?></span>
                                </h4>
                            </article>
                        <?php  endforeach; endif; ?>
                    </div>
                    <div class="tratamentos-texto">
                         <?= apply_filters('the_content', $section->post_content); ?>
                    </div>
                    
                </div>
            </div>
        </section>
        <section id="clientes">
            <header class="row">
                <div class="columns large-12">
                    <h3>Clientes</h3>
                </div>
            </header>
            <div class="row">
                <div class="columns large-12 clientes">
                    <?php
                        $clientes = get_posts(array(
                            'numberposts'	=> -1,
                            'post_type'	=> 'cliente'
                        ));
                    ?>
                    <?php if($clientes): ?>
                        <?php foreach($clientes as $cliente): ?>
                            <article>
                                    <img class="lazy-img" src="<?php bloginfo('template_url'); ?>/_public/img/loading.gif" data-src="<?= wp_get_attachment_url( get_post_thumbnail_id($cliente->ID) ); ?>" alt="<?= $cliente->post_title; ?>" />
                            </article>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        
        <?php get_template_part( 'content', 'loja' ); // elemento compartilhado servicos ?>
        
        <section id="contato">
            <?php $section = get_page_by_path('entre-em-contato',OBJECT,'conteudo'); ?>
            <?php $dados_institucionais = get_page_by_path('dados-institucionais',OBJECT,'conteudo'); ?>
            <header class="row">
                <div class="columns large-12">
                    <h3><?= $section->post_title; ?></h3>
                </div>
            </header>
            <div class="row">
                <div class="columns large-offset-1 large-4">
                    <span class="dados-label">Endereço</span>
                    <span class="dados-contato">
                        <?= get_field("endereco", $dados_institucionais->ID); ?>
                    </span>
                    <span class="dados-label">Telefone</span>
                    <span class="dados-contato">
                        <?= get_field("telefone", $dados_institucionais->ID); ?>
                    </span>
                    <span class="dados-label">Email</span>
                    <span class="dados-contato">
                        <?= get_field("email", $dados_institucionais->ID); ?>
                    </span>
                    <div class="redes-sociais">
                        <a href="<?= get_field("link_google_plus", $dados_institucionais->ID); ?>" class="gplus"><span>G+</span></a>
                        <a href="<?= get_field("link_pintrest", $dados_institucionais->ID); ?>" class="pintrest"><span>Pintrest</span></a>
                        <a href="<?= get_field("link_facebook", $dados_institucionais->ID); ?>" class="facebook"><span>Facebook</span></a>
                        <!--a href="<?= get_field("link_twitter", $dados_institucionais->ID); ?>" class="twitter"><span>Twitter</span></a-->
                    </div>
                </div>
                <div class="columns large-6 end contato-form">
                    <?= apply_filters('the_content', $section->post_content); ?>
                </div>
            </div>
        </section>
        
        <section class="mapa">
            <?php $section = get_page_by_path('mapa',OBJECT,'conteudo'); ?>
            <?php $mapa = get_field("mapa", $section->ID); ?>
             <div class="acf-map">
                <div class="marker" data-lat="<?php echo $mapa['lat']; ?>" data-lng="<?php echo $mapa['lng']; ?>"></div>
            </div>
        </section>
        <style>
                .bannerArea{
                    position: relative;
                }
                    .bannerArea:before{
                        content: '';
                        display: block;
                        width: 100%;
                        height: 100%;
                        position: absolute;
                        z-index: 90;
                        top: 0;
                        left: 0;
                        background: #000;
                        opacity: 0.65;
                    }
                    .bannerArea .bannerTopo{
                        
                    }
                        .bannerArea .bannerTopo .bannerTopo-conteudo{
                         z-index: 100 !important;
                         padding-top: 231px;
                         position: absolute;
                         width: 100%;
                         text-align: center;
                        }   
                            .bannerArea .bannerTopo .bannerTopo-conteudo h2{
                                color: #d4b379;
                                font-size: 3.5em;
                                width: 100%;
                                text-align: center;
                            } 
                            .bannerArea .bannerTopo .bannerTopo-conteudo p{
                             display: block;
                             width: 100%;
                             color: #fff;
                             text-align: center;
                             font-size: 1.5em;                           
                         }  
                            .bannerArea .bannerTopo img{
                                display: block;
                                width: 100%;
                            }  
                @media(max-width: 991px){
                    .bannerArea .bannerTopo .bannerTopo-conteudo{
                        display: none;
                    }
                }
            </style>
        <?php get_footer(); ?>

    </body>
</html>

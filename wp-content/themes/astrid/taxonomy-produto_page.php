<!doctype html>
<html class="no-js" lang="en">
    <head>
        <?php get_header(); ?>
    </head>
    <body>
        <header class="internas">
            <?php get_template_part( 'content', 'responsive-menu' );  ?>
            
            <?php get_template_part( 'content', 'top-bar' );  ?>
            
            <?php get_template_part( 'content', 'menu_interno' ); // elemento compartilhado servicos ?>
        </header>
        <section id="produtos">
            <header class="section-header">
                <?php get_template_part( 'content', 'menu-produtos' ); // elemento compartilhado servicos ?>
            </header>
            <div class="row produtos">
                <?php get_template_part( 'content', 'produto-loop' ); // elemento compartilhado servicos ?>
            </div>
        </section>
        
        <?php get_template_part( 'content', 'loja' ); // elemento compartilhado servicos ?>
        
        <?php get_footer(); ?>
    </body>
</html>



<!doctype html>
<html class="no-js" lang="en">
    <head>
        <?php get_header(); ?>
    </head>
    <body>
        <header class="internas">
            <?php get_template_part( 'content', 'responsive-menu' );  ?>
            
            <?php get_template_part( 'content', 'top-bar' );  ?>
            
            <?php get_template_part( 'content', 'menu_interno' ); // elemento compartilhado servicos ?>
        </header>
        <section id="quem-somos">
            <div class="row">
                <div class="columns large-10 large-offset-1">
                    
                    <?php /* HISTÓRIA */ ?>
                    <?php $section = get_page_by_path('historia',OBJECT,'conteudo'); ?>
                    <h3><?= $section->post_title ?></h3>
                    
                    <div class="texto-colunas">
                        <?= apply_filters('the_content', $section->post_content); ?>
                    </div>
                    
                    <?php /*Fábrica de Bijus */ ?>
                    <?php $section = get_page_by_path('fabrica-de-bijus',OBJECT,'conteudo'); ?>
                    <h3><?= $section->post_title ?></h3>
                    
                    <div class="texto-colunas">
                        <?= apply_filters('the_content', $section->post_content); ?>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="clientes_interna">
            <div class="row">
                <div class="columns large-10 large-offset-1">
                    <h3>Clientes</h3>
                </div>
            </div>
            <div class="row clientes">
                <?php
                    $clientes = get_posts(array(
                        'numberposts'	=> -1,
                        'post_type'	=> 'cliente'
                    ));
                ?>
                <?php if($clientes): ?>
                    <?php foreach($clientes as $cliente): ?>
                        <article class="columns large-2">
                            <figure>
                                <img src="<?= wp_get_attachment_url( get_post_thumbnail_id($cliente->ID) ); ?>" alt="<?= $cliente->post_title; ?>" />
                            </figure>
                        </article>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </section>
        
        <section id="selo_500">
            <div class="row">
                <div class="columns large-11 large-offset-1">
                    <?php $section = get_page_by_path('um-milhao',OBJECT,'conteudo'); ?>
                    <img src="<?= wp_get_attachment_url( get_post_thumbnail_id($section->ID) ); ?>" alt="<?= $section->post_title; ?>">
                    <div class="selo-texto">
                        <?= apply_filters('the_content', $section->post_content); ?>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="principios">
            <div class="row">
                <article class="columns large-4">
                    <?php $section = get_page_by_path('missao',OBJECT,'conteudo'); ?>
                    <h4><?= $section->post_title; ?></h4>
                    <p>
                        <?= $section->post_content; ?>
                    </p>
                 </article>    
                 <article class="columns large-4">
                    <?php $section = get_page_by_path('visao',OBJECT,'conteudo'); ?>
                    <h4><?= $section->post_title; ?></h4>
                    <p>
                        <?= $section->post_content; ?>
                    </p>
                 </article>    
                 <article class="columns large-4">
                    <?php $section = get_page_by_path('valores',OBJECT,'conteudo'); ?>
                    <h4><?= $section->post_title; ?></h4>
                    <p>
                        <?= $section->post_content; ?>
                    </p>
                 </article>   
            </div>
        </section>
        
        <?php get_template_part( 'content', 'loja' ); // elemento compartilhado servicos ?>
        
        <?php get_footer(); ?>
    </body>
</html>



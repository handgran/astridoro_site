<div class="responsive-menu show-for-small-only">
    <div class="row">
        <div class="columns small-10">
                <img src="<?php bloginfo('template_url'); ?>/_public/img/logo-menu-responsive.png" />
        </div>
        <div class="columns small-2">
            <a href="#" class="responsive-menu-call"><i class="fa fa-navicon"></i></a>
        </div>
    </div>
    <div class="row">
        <div class="columns small-12">
            <nav>
                <div class="responsive-menu-mask">
                    <a href="http://www.astridoro.com.br">Principal</a>
                    <a href="http://www.astridoro.com.br/quem-somos/">Quem somos</a>
                    <a href="http://www.astridoro.com.br/produtos/">Produtos</a>
                    <a href="http://www.astridoro.com.br/FAQ/">Suporte</a>
                    <a href="#">Loja Astrid’Oro BIJOUX</a>
                    <a href="http://www.astridoro.com.br/orcamento/">Orçamento</a>
                    <a href="http://www.astridoro.com.br/#contato">Contato</a>
                </div>
            </nav>
        </div>
    </div>
</div>
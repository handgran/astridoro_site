<div class="top-bar">
    <?php $dados_institucionais = get_page_by_path('dados-institucionais',OBJECT,'conteudo'); ?>
    <div class="row">
        <div class="columns large-5 horarios"><i class="fa fa-clock-o"></i> <?= get_field("horarios", $dados_institucionais->ID); ?> <span>/</span> <?= get_field("endereço_topo", $dados_institucionais->ID); ?></div>
        <div class="columns large-5 contato">
            <i class="fa fa-envelope-o"></i> <?= get_field("email", $dados_institucionais->ID); ?> <span> / </span> 
            <?= get_field("telefone", $dados_institucionais->ID); ?> <span> / </span> 
            <a href="https://www.facebook.com/astridoro" target="_blank" ><i class="fa fa-facebook"></i></a>



            <!--a href="<?= get_field("link_pintrest", $dados_institucionais->ID); ?>"><i class="fa fa-pinterest"></i></a-->
        </div>
    </div>
</div>


<!doctype html>
<html class="no-js" lang="en">
    <head>
        <?php get_header(); ?>
    </head>
    <body>
        <header class="internas">
            <?php get_template_part( 'content', 'responsive-menu' );  ?>
            
            <?php get_template_part( 'content', 'top-bar' );  ?>
            
            <?php get_template_part( 'content', 'menu_interno' ); // elemento compartilhado servicos ?>
        </header>
        <section id="FAQ">
            <div class="row">
                <div class="collumns large-8 large-offset-2">
                    <?php $section = get_page_by_path('como-enviar-sua-arte',OBJECT,'conteudo'); ?>
                    <h3><?= $section->post_title ?></h3>
                    <?= apply_filters('the_content', $section->post_content); ?>
                </div>
                
            </div>
            
        </section>
        
        <?php get_template_part( 'content', 'orcamento' ); // elemento compartilhado servicos ?>
        
        <?php get_template_part( 'content', 'loja' ); // elemento compartilhado servicos ?>

        <?php get_footer(); ?>
    </body>
</html>



<!doctype html>
<html class="no-js" lang="en">
    <head>
        <?php get_header(); ?>
    </head>
    <body>
        <header class="internas">
            <?php get_template_part( 'content', 'responsive-menu' );  ?>
            
            <?php get_template_part( 'content', 'top-bar' );  ?>
            
            <?php get_template_part( 'content', 'menu_interno' ); // elemento compartilhado servicos ?>
        </header>
        <section id="obrigado">
            <div class="row">
                <div class="columns large-offset-3 large-6 small-12">
                     <?php $section = get_page_by_path('obrigado',OBJECT,'conteudo'); ?>
                    <h3><?= $section->post_title ?></h3>
                    <p><?= $section->post_content ?></p>

                    <a href="#">Continue</a>
                    
                </div>
            </div>
        </section>
        
        <?php get_template_part( 'content', 'loja' ); // elemento compartilhado servicos ?>
        
        <?php get_footer(); ?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5CXHTK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5CXHTK');</script>
<!-- End Google Tag Manager -->

    </body>
</html>


$(document).foundation();
$(".fancybox-skin").append("<a href='http://www.astridoro.com.br/orcamento/' class='btnOrcamento'>Orçamento</a>");
function add_marker($marker, map) {

    // var
    var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

    // create marker
    var marker = new google.maps.Marker({
        position: latlng,
        map: map
    });

    // add to array
    map.markers.push(marker);

    // if marker contains HTML, add it to an infoWindow
    if ($marker.html())
    {
        // create info window
        var infowindow = new google.maps.InfoWindow({
            content: $marker.html()
        });

        // show info window when marker is clicked
        google.maps.event.addListener(marker, 'click', function () {

            infowindow.open(map, marker);

        });
    }

}

/*
 *  center_map
 *
 *  This function will center the map, showing all markers attached to this map
 *
 *  @type	function
 *  @date	8/11/2013
 *  @since	4.3.0
 *
 *  @param	map (Google Map object)
 *  @return	n/a
 */

function center_map(map) {

    // vars
    var bounds = new google.maps.LatLngBounds();

    // loop through all markers and create bounds
    $.each(map.markers, function (i, marker) {

        var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

        bounds.extend(latlng);

    });

    // only 1 marker?
    if (map.markers.length == 1)
    {
        // set center of map
        map.setCenter(bounds.getCenter());
        map.setZoom(16);
    }
    else
    {
        // fit to bounds
        map.fitBounds(bounds);
    }

}

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;


$(document).ready(function(){
    
   $('.acf-map').each(function(){
            map = new_map( $(this) );
   });
    
   if($(".produtos article").length > 0)
   {
       $(".produtos article").last().addClass("end");
   }
   
   if($(".clientes article").length > 0)
   {
       $(".clientes article").last().addClass("end");
   }
   
   
   $(".checks label").click(function(){
       console.log($(this).prev().is(":Checked"));
       if($(this).prev().is(":Checked"))
       {   
           $(this).prev().is(":Checked", false);
           $(this).children("i").addClass("fa-square-o");
           $(this).children("i").removeClass("fa-check-square-o");
       } else {
           $(this).prev().is(":Checked", true);
           $(this).children("i").removeClass("fa-square-o");
           $(this).children("i").addClass("fa-check-square-o");
       }
       
        
      
   });
   var bLazy;
   
   bLazy = new Blazy({ 
        selector: '.lazy-img' // all images
   });
   
   
    $(".banners").bxSlider({
        mode: 'fade',
        auto: true,
        autoControls: false,
        pause: 5000,
        pager: false,
        controls:false,
        height:536,
        onSliderLoad: function(){
            bLazy.revalidate();
        }
    });
    
    
    $('#clientes .clientes').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      arrows: false,
      speed: 300,
      autoplay: true,
      autoplaySpeed: 2000
    });
    
    $('#clientes .clientes').on('afterChange', function(){
        bLazy.revalidate();
    });
    
    $(".responsive-menu-call").click(function(){
        if($(".responsive-menu nav").height() == 0)
        {
            $(".responsive-menu nav").animate({
                height: $(".responsive-menu nav .responsive-menu-mask").height()
            });
        } else {
            $(".responsive-menu nav").animate({
                height: 0
            });
        }
    });
    
    $(".fancy").fancybox({
    	openEffect	: 'elastic',
    	closeEffect	: 'elastic',

    	helpers : {
    		title : {
    			type : 'inside'
    		}
    	}
    });
});


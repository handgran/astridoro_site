   
    <?php $categoria_produtos = get_terms('produto_page') ?>
    <?php if ($categoria_produtos): ?>            
        

<div class="categoria">
	<div class="todos">
		 <a href="<?php echo get_home_url() ?>/produtos/" class="<?= (!is_tax( "produto_page"))? "active": ""; ?>">Todos</a>
	</div>

	<div class="outros">
		<ul>
			<?php foreach ($categoria_produtos as $categoria): ?>
			<li>
				<a href="<?= get_term_link($categoria) ?>" class="<?= (is_tax( "produto_page", $categoria->term_id ))? "active": ""; ?>"><?= $categoria->name ?></a>
			</li>
			<?php endforeach;?>
		</ul>
	</div>

</div>
  <?php endif; ?>
    


<style>
	
header.section-header {
    margin-bottom: 55px;
    padding: 30px 0;
}

	
	.categoria{
		max-width: 750px;
        margin: 0 auto;
        height: 92px;


	}

		 .categoria .todos{
		    margin: 0;
		    padding: 0;
		    width: 100%;
		    max-width: 90px;
		    display: inline-block;
		    text-align: center;
		    height: 90px;
		    margin: 0;
		    line-height: 80px;
		    float: left;
				}

			.categoria .todos a{
			    color: #202020;
			    font-size: 1.33em;
			    border-bottom: 2px solid #0000; 

	 		}


			.categoria .todos a:hover { 
				border-color: #d4b379; 
			}

		.categoria .outros{
			margin: 0;
		    padding: 0;
		    /* width: 100%; */
		    /* max-width: 647px; */
		    display: inline-block;
		    float: right;
								
		}
			.categoria .outros ul{
			padding: 0;
		    font-size: 0;
		    max-width: 660px;
		    display: block;
						
			}
				 .categoria .outros ul li{
				    text-align: center;
				    padding: 10px 20px;
				    margin: 0;
				    display: inline-block;
				    width: 100%;
				    max-width: 101px;
				    vertical-align: top;
				    font-size: 11px;


							
				}
				   .categoria .outros ul li a{
						display: inline-block;
					    color: #202020;
					    border-bottom: 2px solid #0000;
					    font-size: 1.33em;
					}

					.categoria  .outros ul li  a:hover { 
			         border-color: #d4b379; 
			}


</style>
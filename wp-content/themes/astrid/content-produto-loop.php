<?php while (have_posts()) : the_post(); ?>
    <article class="columns large-3">
        <a href="<?= wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>" class="fancy" title="<?= get_the_title() ?>">
            <figure>
                <!--img class="lazy-img" src="" data-src="<?= wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()) , "produto_thumb"); ?>" alt="<?= get_the_title() ?>" /-->
                <?= the_post_thumbnail ('produto_thumb') ?>
            </figure>
        </a>
    </article>

<?php endwhile; ?>

<script>
	$( ".fancy" ).click(function(e) {
		$(".fancybox-skin").append("<a href='http://www.astridoro.com.br/orcamento/' class='btnOrcamento'>Orçamento</a>");
	});
	
</script>
<nav class="menu-principal">
                <div class="row">
                    <div class="columns large-3">
                        <a href="http://www.astridoro.com.br"><img src="<?php bloginfo('template_url'); ?>/_public/img/astrid-oro.png" /></a>
                    </div>
                    <div class="columns large-9 ajusta-alinhamento-menu">
                        <a href="http://www.astridoro.com.br" class="<?= (is_home())? "active": ""; ?>">Principal</a>
                        <a href="http://www.astridoro.com.br/quem-somos/" class="<?= (is_page("quem-somos"))? "active": ""; ?>">Quem somos</a>
                        <a href="http://www.astridoro.com.br/produtos/"  class="<?= (is_page("produtos") || is_tax( "produto_page"))? "active": ""; ?>">Produtos</a>
                        <a href="http://www.astridoro.com.br/FAQ/"  class="<?= (is_page("FAQ"))? "active": ""; ?>">Suporte</a>
                       
                        <a href="http://www.astridoro.com.br/orcamento/"  class="<?= (is_page("orcamento"))? "active": ""; ?>">Orçamento</a>
                        <a href="http://www.astridoro.com.br/#contato"  class="<?= (is_home())? "active": ""; ?>">Contato</a>
                    </div>
                </div>
            </nav>
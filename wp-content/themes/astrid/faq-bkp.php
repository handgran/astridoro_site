<h5>ENTENDENDO O PROCESSO </h5>
                    <p>
                        O processo de fundição das peças inicia-se com a confecção de clichês elaborados com base nos fotolitos gerados em bureaus especializados. Portanto, todas os produtos são cuidadosamente redesenhados em programas de desenho vetorial, com base nos originais fornecidos pelo cliente.
                    </p>
                    
                    <h5>ENVIANDO SUAS IMAGENS</h5>
                    <p>
                        Para que as peças saiam com desenhos perfeitos é preciso que o material a nós remetido contenha a melhor resolução possível, com traços, cores e contornos bem definidos.
A seguir estão algumas orientações para as dúvidas mais frequentes:
                    </p>
                    
                    <h5>1. Tipos de arquivo</h5>
                    <p>
                                                Use preferencialmente arquivos .gif ou .jpg. Esses formatos foram criados para facilitar o envio eletrônico de arquivos. Eles permitem grande grau de compactação, economizando uma quantidade considerável de bytes. 
                        Utilize .gif quando se tratar de imagens com poucas cores como logotipos e artes em preto e branco e .jpg para imagens coloridas em geral. 
                        Importante:
                        Anexe as imagens diretamente no email. 
                        Evite "colar" as imagens em aplicativos como Word e Corel Draw. Quando você copia e cola, além de perder resolução, o tamanho do arquivo fica bem mais pesado.
                    </p>

                    <h5>2. Resolução ideal para digitalizar uma imagem </h5>
                    <p>
                        No nosso caso, em que nos baseamos no vídeo e na impressão a jato de tinta, 150 dpis é resolução suficiente para que tenhamos boas informações.
                        Portanto, evite utilizar resolução maior que esta. Estaríamos aumentando desnecessariamente o tamanho do arquivo.
                    </p>

                    <h5>3. Artes criadas no Corel Draw</h5>
                    <p>
                        Quando se tratar de arquivos criados no próprio Corel Draw, envie o arquivo original .cdr.
                    </p>

                    <h5>4. Envio de imagem via Fax</h5>
                    <p>
                        A ausência de cores e o baixo contraste gerado por alguns equipamentos, muitas vezes impossibilitam o redesenho. 
                        Para evitar questões com esta, observar: 
                        - Testar inicialmente na sua empresa se a qualidade da imagem é legível.
                        - Atentar para que todos os detalhes permaneçam íntegros.
                        - Mostrar corretamente todos os campos coloridos por meio de uma flexa, com a indicação do tom desejado.
                    </p>
                    
                    <h4>UM POUCO MAIS SOBRE IMAGENS</h4>
                    
                    <h5>Arquivos JPEG</h5>
                    <p>
                        Cuidado na hora de definir a compactação no formato .jpg, procurando utilizar um percentual que não destrua os detalhes do desenho. Apesar deste formato permitir altos níveis de compatação, a qualidade cai também na mesma proporção. Níveis aceitáveis estão entre 20% a 40%, ou seja, mantém o arquivo com 60% a 80% do tamanho original.
                        Alguns arquivos podem atingir compactações altíssimas sem perda da qualidade. Cada caso deverá ser analisado individualmente e o julgamento é visual. 
                    </p>

                    <h5>Arquivos GIF</h5>
                    <p>
                        Os arquivos com extensão .gif podem também ficar ainda menores.
                        Na hora de definir o número de cores, pode-se testar a imagem com menos de 256 cores, número total permitido neste tipo de arquivo. Experimente quantidades menores de cores como 156, 128, 64 etc. Muitas vezes as cores de uma arte exigem combinações baixas. diminuindo sensivelmente o tamanho do arquivo sem perda dos detalhes. 
                    </p>

                    <h5>Melhorando a definição da imagem</h5>
                    <p>
                        O uso de ferramentas como Photoshop e Photopaint pode melhorar significativamente a qualidade da imagem. Se você puder, utilize recursos como brilho, contraste e equilíbrio de cores.
                        O recurso Unsharp Mask, quando bem dosado, pode melhorar em muito a nitidez da imagem.
                        </p>
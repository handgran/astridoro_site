<!doctype html>
<html class="no-js" lang="en">
    <head>
        <?php get_header(); ?>
    </head>
    <body>
        <header class="internas">
            <?php get_template_part( 'content', 'responsive-menu' );  ?>
            
            <?php get_template_part( 'content', 'top-bar' );  ?>
            
            <?php get_template_part( 'content', 'menu_interno' ); // elemento compartilhado servicos ?>
        </header>
        <section id="formulario-orcamento">
            <div class="row">
                <div class="columns large-4 large-offset-1">
                    <figure class="logo">
                        <img src="<?php bloginfo('template_url'); ?>/_public/img/astrid-oro.png">
                    </figure>
                    <?php $dados_institucionais = get_page_by_path('dados-institucionais',OBJECT,'conteudo'); ?>
                    <span class="dados-label">Endereço</span>
                    <span class="dados-contato">
                        <?= get_field("endereco", $dados_institucionais->ID); ?>
                    </span>
                    <span class="dados-label">Telefone</span>
                    <span class="dados-contato">
                         <?= get_field("telefone", $dados_institucionais->ID); ?>
                    </span>
                    <span class="dados-label">Email</span>
                    <span class="dados-contato">
                        <?= get_field("email", $dados_institucionais->ID); ?>
                    </span>
                    <div class="redes-sociais">
                        <span class="dados-label">Redes sociais</span>
                        <!--a href="<?= get_field("link_pintrest", $dados_institucionais->ID); ?>" class="pintrest"><span>Pintrest</span></a-->
                        <a href="https://www.facebook.com/astridoro" class="facebook" target="_blank"><span>Facebook</span></a>
                    </div>
                    
                   <!--  <div class="selo">
                        <img class="lazy-img" src="" data-src="<?= wp_get_attachment_url( get_post_thumbnail_id($dados_institucionais->ID) ); ?>" alt="Selo 30 anos" />
                    </div> -->
                </div>
                <div class="columns large-6 end">
                    <?php $section = get_page_by_path('dados-para-orcamento',OBJECT,'conteudo'); ?>
                    <h3><?= $section->post_title; ?></h3>
                    <!--span class="preencha">Preencha todos os campos com dados da sua empresa</span-->
                    <?= apply_filters('the_content', $section->post_content); ?>
                </div>
            </div>
        </section>
        
        <?php get_template_part( 'content', 'loja' ); // elemento compartilhado servicos ?>
        
        <?php get_footer(); ?>


<!-- Google Code for Contato/Or&ccedil;amento Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 927753140;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "awvtCNmPkWYQtMexugM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/927753140/?label=awvtCNmPkWYQtMexugM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


    </body>
</html>



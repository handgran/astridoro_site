<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Astrid'Oro</title>

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600italic,700italic,800,800italic,700,600' rel='stylesheet' type='text/css'>
        
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/_public/css/foundation.css" />
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/_public/css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/_public/css/app.css" />
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php bloginfo('template_url'); ?>/_public/js/vendor/modernizr.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/_public/js/vendor/jquery.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/_public/js/foundation.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/_public/js/vendor/blazy.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/_public/js/vendor/jquery.bxslider.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/_public/js/vendor/slick.min.js"></script>
        <link href="<?php bloginfo('template_url'); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <!-- Add fancyBox -->
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/_public/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/_public/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
        
        <script src="<?php bloginfo('template_url'); ?>/_public/js/app.js"></script>
        <script>
            $(document).foundation();
        </script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        
       
        <?php wp_head(); ?>